Entrega 7

El commit a corregir es: 9d7bf6b, con el mensaje "Entrega 7 Actualizada".

Sobre JUnit: Se deben ejecutar los 4 Tests que est�n en el package Test.MainTests. 
Los primeros 3 tests est�n arreglados (pueden demorar un par de segundos en ejecutarse), 
aunque el cuarto a�n nos tira un null pointer exception.

Sobre las Migraciones y Estad�sticas: El portal de la izquierda es el portal de salida, 
y el de la derecha el de llegada. Si no hay espacio para recibir una entidad, esta se mete 
en una queue hasta que haya espacio para colocarla. De esta manera, el marcador puede que 
muestre que hay menos unidades que las iniciales, pero eso se deber�a a que hay entidades 
esperando a ser insertadas, y una vez hecho eso, el marcador volver� a incrementar.

Sobre las Entidades: El tama�o y forma de las entidades determinan su superficie de contacto, 
lo cual, seg�n el Documento de Visi�n 2 (Entrega 2), determinar�an su capacidad de convencer 
o ser convencidos. Esto hace que sea m�s acorde a la realidad, puesto que no todas las personas 
piensan de la misma forma. Los muros, por otra parte, pueden representar barreras culturales, 
puesto que en la realidad siempre habr�n nichos de personas ubicadas en distintos lugares.

En esta entrega se trat� de pulir la entrega anterior, ya que esta, para nosotros, ten�a un car�cter 
un poco m�s definitivo.

- En la carpeta C�digo se encuentra:
	La carpeta LifePolitica, que contiene al proyecto.
	
- En la carpeta Docs se encuentra la carpeta Entrega 7, que contiene:
	Los diagramas UML.
	Screenshots y an�lisis de Metrics.
	Screenshots y an�lisis de JProfiler.

- Santiago Mart�:
	JUnit.

- Guillermo Ibacache:
	Arreglar errores de concurrencias.
	Implementar properties para manejar las migraciones (en vez de Hardcodear los IPs).	

- Francesca Garziera:
	UML, Metrics, JProfiler.			

- Sebasti�n Salata:
	Arreglar bugs en las estad�sticas.	

- Camila Gonz�lez:
	Arreglar Ant.