package Test.ArchivosTest2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Random;

import Test.Backend.ParametrosIniciales;


public class BackendControllerTest2 {

	private SimuladorTest2 simulador;
	private long startTime;
	public int tiempoEspera;

	public static Random random = new Random();
	
	public BackendControllerTest2() {
	}

	public void iniciarSimulacionTest(BackendControllerTest2 bC, int vel, int partidos, HashMap<Integer, Integer> porcs, int tiempoEspera)
	{
		this.tiempoEspera = tiempoEspera;
		ParametrosIniciales.getInstance().setParametros(vel, partidos, porcs);
		InitiateSimulationEventTest2 sim = new InitiateSimulationEventTest2(bC);
		sim.setPercentages(porcs);
		sim.setPartidos(partidos);
		sim.setVelocidad(vel);
		sim.actionPerformed(null);
	}
	public boolean ingresarParametrosIniciales(int vel, int numPart, HashMap<Integer, Integer> porcsParts) {
		return ParametrosIniciales.getInstance().setParametros(vel, numPart, porcsParts);
	}


	public void iniciarSimulacion(int tiempoEspera) {
		SimuladorTest2.iniciarSimulacion(tiempoEspera);
	}

	public void setSimulador(SimuladorTest2 simulador) {
		this.simulador = simulador;
	}

	public void guardarEstado() {
		SimuladorTest2.guardarEstado();
	}

	public SimuladorTest2 getSimulador() {
		return simulador;
	}
	
	public long getStartTime () {
		return startTime;
	}
	
	public void setStartTime (long n) {
		this.startTime = n;
	}
}

class InitiateSimulationEventTest2 implements ActionListener {

	private BackendControllerTest2 backendController;
	
	private HashMap<Integer, Integer> percentages;
	public HashMap<Integer, Integer> getPercentages() {
		return percentages;
	}

	public void setPercentages(HashMap<Integer, Integer> percentages) {
		this.percentages = percentages;
	}

	private int velocidad;
	
	public int getVelocidad() {
		return velocidad;
	}

	public void setVelocidad(int velocidad) {
		this.velocidad = velocidad;
	}

	public int getPartidos() {
		return partidos;
	}

	public void setPartidos(int partidos) {
		this.partidos = partidos;
	}

	private int partidos;
	
	public InitiateSimulationEventTest2(BackendControllerTest2 backendController) {
		this.backendController = backendController;
	}

	public void actionPerformed(ActionEvent e) {
		this.backendController.setStartTime(System.currentTimeMillis());
		this.backendController.iniciarSimulacion(this.backendController.tiempoEspera);
	
	}
}


