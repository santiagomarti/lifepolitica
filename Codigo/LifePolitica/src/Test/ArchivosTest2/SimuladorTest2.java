package Test.ArchivosTest2;

import java.util.ArrayList;

import Test.Backend.NombrePartido;
import Test.Backend.ParametrosIniciales;
import Test.Backend.PartidoPolitico;


public class SimuladorTest2 {

	public static CiudadTest2 ciudad;

	public SimuladorTest2() {

		numIteracion = 0; // Para graficos
	}

	public static void iniciarSimulacion(int tiempoEspera) {
		ciudad = new CiudadTest2(tiempoEspera);
	}

	public static int numIteracion; // Solo para los informes

	public static ArrayList<PartidoPolitico> guardarEstado() {
		// Modificado para crear informe. Arreglar para implementacion final
		// Esto es ya que aun no crean los partidos una vez que los hagan, sacan
		// esto

		ArrayList<PersonaTest2> personas = ciudad.getPersonas();
		ArrayList<PartidoPolitico> partidosEnCiudad = new ArrayList<PartidoPolitico>();

		int numPartidos = ParametrosIniciales.getInstance().getNumeroDePartidos();
		
		PartidoPolitico p1 = new PartidoPolitico(NombrePartido.ComuNAUS);
		PartidoPolitico p2 = new PartidoPolitico(NombrePartido.SoliSoli);
		PartidoPolitico p3 = new PartidoPolitico(NombrePartido.ElGremio);
		PartidoPolitico p4 = new PartidoPolitico(NombrePartido.Creceres);
		
		switch (numPartidos){
		case 1: partidosEnCiudad.add(p1); break;
		case 2: partidosEnCiudad.add(p1); partidosEnCiudad.add(p2); break;
		case 3: partidosEnCiudad.add(p1); partidosEnCiudad.add(p2); partidosEnCiudad.add(p3); break;
		default: partidosEnCiudad.add(p1); partidosEnCiudad.add(p2); partidosEnCiudad.add(p3); partidosEnCiudad.add(p4); break;
		}
		
		for (PersonaTest2 per : personas) {

			if (per.getPartido().equals(p1.getNombre())) {
				p1.setAdherentes(p1.getAdherentes() + 1);
			} else if (per.getPartido().equals(p2.getNombre())) {
				p2.setAdherentes(p2.getAdherentes() + 1);
			} else if (per.getPartido().equals(p3.getNombre())) {
				p3.setAdherentes(p3.getAdherentes() + 1);
			} else if (per.getPartido().equals(p4.getNombre())) {
				p4.setAdherentes(p4.getAdherentes() + 1);
			}
		}

		return partidosEnCiudad;
	}
	

	public static CiudadTest2 getCiudad() {
		return ciudad;
	}

}