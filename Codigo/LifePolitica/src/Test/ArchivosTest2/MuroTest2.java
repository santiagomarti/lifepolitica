package Test.ArchivosTest2;

import java.awt.Point;
import java.util.ArrayList;

import Test.LogicaCiudad.Celda;
import Test.LogicaCiudad.Direccion;
import Test.LogicaCiudad.Entidad;


public class MuroTest2 extends Entidad{

	public MuroTest2(Point start, int length, Direccion direccion){
		this.componentes = new ArrayList<Celda>();
		
		Celda[][] grilla = CiudadTest2.getCeldas();
				
		for (int i = 0; i < length; i++){
			Celda celda = new Celda(start, this);
			
			grilla[start.x][start.y] = celda;
			this.componentes.add(celda);
			
			start = CiudadTest2.movePoint(start, direccion);
		}
	}
	
	@Override
	public String getTipo() {
		return "muro";
	}
}