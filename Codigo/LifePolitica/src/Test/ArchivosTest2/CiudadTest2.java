package Test.ArchivosTest2;

import static org.junit.Assert.*;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import Test.Backend.NombrePartido;
import Test.Backend.ParametrosIniciales;
import Test.LogicaCiudad.Celda;
import Test.LogicaCiudad.Direccion;
import Test.LogicaCiudad.Entidad;
import Test.LogicaCiudad.Persona;


public class CiudadTest2 {

	private static Celda[][] celdas;
	public ArrayList<PersonaTest2> personas = new ArrayList<PersonaTest2>();
	private ArrayList<MuroTest2> muros = new ArrayList<MuroTest2>();

	private ArrayList<Thread> threads = new ArrayList<Thread>();

	public CiudadTest2(int tiempoEspera) {

		celdas = new Celda[40][40];

		this.crearMuros();
		this.crearPersonas();
		this.crearCalles();

		for (PersonaTest2 persona : personas) {
			Thread thread = new Thread(persona);
			this.threads.add(thread);
			thread.start();
		}
		try {
			Thread.sleep(tiempoEspera);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public ArrayList<Thread> getThreads() {
		return threads;
	}

	public void crearPersonas() {
		HashMap<Integer, NombrePartido> partidos = ParametrosIniciales.getInstance()
				.getPartidos();
		HashMap<Integer, Integer> distribucion = ParametrosIniciales
				.getInstance().getPorcentajes();

		for (int id : partidos.keySet()) {
			for (int j = 0; j < distribucion.get(id); j++) {
				PersonaTest2 persona = new PersonaTest2();
				persona.setPartido(partidos.get(id));
				this.personas.add(persona);
			}
		}
	}

	public void crearMuros() {
		for (int i = 0; i < 20; i++) {
			Direccion direccion = Direccion.random();

			int size = BackendControllerTest2.random.nextInt(15) + 1;

			int x = BackendControllerTest2.random.nextInt(celdas.length);
			int y = BackendControllerTest2.random.nextInt(celdas[0].length);

			Point start = new Point(x, y);

			MuroTest2 muro = new MuroTest2(start, size, direccion);
			this.muros.add(muro);
		}
	}

	public void crearCalles() {
		for (int i = 0; i < celdas.length; i++) {
			for (int j = 0; j < celdas[0].length; j++) {
				if (celdas[i][j] == null) {
					Celda celda = new Celda(new Point(i, j), null);
					celdas[i][j] = celda;
				}
			}
		}
	}

	@SuppressWarnings("deprecation")
	public void Stop() {
		for (Thread thread : this.threads) {
			thread.stop();
		}
	}

	public static Celda[][] getCeldas() {
		return celdas;
	}

	/** Mueve una entidad en una direccion. */
	public static synchronized void moverEntidad(Entidad entidad,
			Direccion direccion) {
		boolean seMovio = false;
		boolean podiaMoverse = hayCeldaVacia(entidad, direccion);
		List<Celda> componentesEntidad = entidad.componentes;
		List<Celda> celdasVecinasDireccion = new ArrayList<Celda>();

		for (Celda celda : componentesEntidad) {
			Celda celdaVecina = getCeldaVecina(celda, direccion);
			if (celdaVecina.getParent() != null) {
				celdasVecinasDireccion.add(celdaVecina);
			}
		}

		celdasVecinasDireccion.removeAll(componentesEntidad);

		if (celdasVecinasDireccion.isEmpty()) {
			seMovio = true;
			for (Celda celda : componentesEntidad) {
				moverCelda(celda, direccion);
			}
		}
		assertEquals(seMovio, podiaMoverse);
	}
	
	public static boolean hayCeldaVacia(Entidad e, Direccion d)
	{
		List<Celda> componentesEntidad = e.componentes;
		List<Celda> celdasVecinasDireccion = new ArrayList<Celda>();

		for (Celda celda : componentesEntidad) {
			Celda celdaVecina = getCeldaVecina(celda, d);
			if (celdaVecina.getParent() != null) {
				celdasVecinasDireccion.add(celdaVecina);
			}
		}
		celdasVecinasDireccion.removeAll(componentesEntidad);
		return celdasVecinasDireccion.isEmpty();	
	}

	/** Mueve una Celda en una direccion. */
	private static void moverCelda(Celda celda, Direccion direccion) {
		Point oldPoint = celda.getPoint();
		Point newPoint = movePoint(oldPoint, direccion);
		celda.setPoint(newPoint);

		int oldX = oldPoint.x;
		int oldY = oldPoint.y;
		int newX = newPoint.x;
		int newY = newPoint.y;

		celdas[newX][newY] = celda;
		if (celdas[oldX][oldY].equals(celda)) {
			celdas[oldX][oldY] = new Celda(new Point(oldX, oldY), null);
		}

	}

	/** Retorna un nuevo punto trasladado.*/
	public static Point movePoint(Point point, Direccion direccion) {
		int y = point.y;
		int x = point.x;
		int maxY = celdas[0].length - 1;
		int maxX = celdas.length - 1;

		switch (direccion) {
		case Up:
			if (y == 0)
				y = maxY;
			else
				y = y - 1;
			break;

		case Down:
			if (y == maxY)
				y = 0;
			else
				y = y + 1;
			break;

		case Right:
			if (x == maxX)
				x = 0;
			else
				x = x + 1;
			break;

		case Left:
			if (x == 0)
				x = maxX;
			else
				x = x - 1;
			break;
		}

		return new Point(x, y);
	}

	/** Retorna la celda vecina a otra en una direccion. */
	public static Celda getCeldaVecina(Celda celda, Direccion direccion) {
		Point puntoVecino = movePoint(celda.getPoint(), direccion);
		int x = puntoVecino.x;
		int y = puntoVecino.y;
		return celdas[x][y];
	}

	/** Revisa las celdas vecinas de una Persona y la cambia de partido. */
	public static synchronized void revisarVecinosPersona(PersonaTest2 persona) {
		Set<Celda> celdasVecinasEntidad = celdasVecinasPersona(persona);
		double totalCeldasVecinas = celdasVecinasEntidad.size();

		HashMap<NombrePartido, Integer> mapPartidoCantidad = contarPartidos(celdasVecinasEntidad);

		NombrePartido nuevoPartido = obtenerNuevoPartido(mapPartidoCantidad,
				totalCeldasVecinas, persona);
		
		if (nuevoPartido.getCode() != persona.getNumeroPartido()){
			int porj = ParametrosIniciales.getInstance().getPorcentajes().get(persona.getNumeroPartido());
			ParametrosIniciales.getInstance().setPorcentaje(persona.getNumeroPartido(), porj-1);
			
			porj = ParametrosIniciales.getInstance().getPorcentajes().get(nuevoPartido.getCode());
			ParametrosIniciales.getInstance().setPorcentaje(nuevoPartido.getCode(), porj+1);
			
			persona.setPartido(nuevoPartido);
		}
	}

	/** Calcula y retorna el partido al cual se debe cambiar la Persona. */
	private static NombrePartido obtenerNuevoPartido(
			HashMap<NombrePartido, Integer> mapPartidoCantidad, double totalVecinos,
			PersonaTest2 persona) {

		NombrePartido partidoActual= persona.getPartido();
		double porcentaje;
		porcentaje = mapPartidoCantidad.get(partidoActual) / totalVecinos;
		if (porcentaje > 0.8) {
			persona.setTicksPartidoBloqueado(60);
			return ParametrosIniciales.getInstance().getPartidoRandom();
		}
		
		for (NombrePartido partido : mapPartidoCantidad.keySet()) {
			if (!partido.equals(partidoActual)) {
				porcentaje = mapPartidoCantidad.get(partido) / totalVecinos;
				if (porcentaje >= 0.25 && BackendControllerTest2.random.nextInt(2) == 0) {
					persona.setTicksPartidoBloqueado(60);
					return partido;
				}
			}
		}

		return partidoActual;
	}

	/** Retorna un hashmap con la cantidad de partidos vecinos. */
	private static HashMap<NombrePartido, Integer> contarPartidos(
			Set<Celda> vecinosEntidad) {

		HashMap<NombrePartido, Integer> mapPartidoCantidad = new HashMap<NombrePartido, Integer>();

		for (NombrePartido partido : ParametrosIniciales.getInstance().getPartidos()
				.values()) {
			mapPartidoCantidad.put(partido, 0);
		}

		for (Celda vecino : vecinosEntidad) {
			if (vecino.getParent() instanceof Persona) {
				Persona personaVecina = (Persona) vecino.getParent();
				NombrePartido partidoVecino = personaVecina.getPartido();
				int cuentaActual = mapPartidoCantidad.get(partidoVecino);
				mapPartidoCantidad.put(partidoVecino, cuentaActual + 1);
			}
		}

		return mapPartidoCantidad;
	}

	/** Retorna las celdas vecinas de una Persona. */
	private static Set<Celda> celdasVecinasPersona(PersonaTest2 persona) {
		Set<Celda> vecinos = new HashSet<Celda>();

		for (Celda componente : persona.componentes) {
			vecinos.addAll(celdasVecinasCelda(componente));
		}
		
		vecinos.removeAll(persona.componentes);

		return vecinos;
	}

	/** Retorna las celdas vecinas de una celda */
	private static Set<Celda> celdasVecinasCelda(Celda componente) {
		Set<Celda> vecinos = new HashSet<Celda>();

		for (Direccion dir : Direccion.values()) {
			vecinos.add(getCeldaVecina(componente, dir));
		}

		return vecinos;
	}
	
	public ArrayList<PersonaTest2> getPersonas() {
		return personas;
	}

	public void setPersonas(ArrayList<PersonaTest2> personas) {
		this.personas = personas;
	}
}