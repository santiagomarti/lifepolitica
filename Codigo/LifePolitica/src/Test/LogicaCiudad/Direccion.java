package Test.LogicaCiudad;

import Test.Backend.BackendController;

public enum Direccion {

	Up, Down, Left, Right;

	public static Direccion random() {
		int probability = BackendController.random.nextInt(4);

		switch (probability) {
		case 0:
			return Direccion.Up;
		case 1:
			return Direccion.Down;
		case 2:
			return Direccion.Right;
		default:
			return Direccion.Left;
		}
	}
}
