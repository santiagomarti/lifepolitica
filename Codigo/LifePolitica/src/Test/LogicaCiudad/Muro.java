package Test.LogicaCiudad;

import java.awt.Point;
import java.util.ArrayList;

public class Muro extends Entidad{

	public Muro(Point start, int length, Direccion direccion){
		this.componentes = new ArrayList<Celda>();
		
		Celda[][] grilla = Ciudad.getCeldas();
				
		for (int i = 0; i < length; i++){
			Celda celda = new Celda(start, this);
			
			grilla[start.x][start.y] = celda;
			this.componentes.add(celda);
			
			start = Ciudad.movePoint(start, direccion);
		}
	}
	
	@Override
	public String getTipo() {
		return "muro";
	}
}