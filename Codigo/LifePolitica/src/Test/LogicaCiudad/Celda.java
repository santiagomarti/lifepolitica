package Test.LogicaCiudad;

import java.awt.Point;

public class Celda {
	private Point point;
	private Entidad parent;

	public Celda(Point point, Entidad e) {
		this.setPoint(point);
		this.parent = e;
	}

	public Entidad getParent() {
		return this.parent;
	}

	public Point getPoint() {
		return point;
	}

	public void setPoint(Point point) {
		this.point = point;
	}
}