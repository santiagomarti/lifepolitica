package Test.LogicaCiudad;

import java.awt.Point;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import Test.Backend.BackendController;
import Test.Backend.NombrePartido;
import Test.Backend.ParametrosIniciales;


public class Persona extends Entidad implements Runnable {

	private NombrePartido partido;

	private double tick;
	private int ticksPartidoBloqueado;
	
	public Persona() {
		this.componentes = new ArrayList<Celda>();
		
		this.tick = 100 + (BackendController.random.nextDouble())
				* (500 - 5 * ParametrosIniciales.getInstance().getVelocidad());

		this.placeComponentes();
	}

	@Override
	public void run() {
		while (true) {
			this.pausar();
			this.revisarVecinos();
			this.mover();
		}
	}

	private void pausar() {
		try {
			TimeUnit.MILLISECONDS.sleep((long) tick);
			System.out.println("Estoy esperando " + this.toString());
		} catch (InterruptedException e) {
		}
	}

	private void revisarVecinos() {
		if (ticksPartidoBloqueado == 0) {
			Ciudad.revisarVecinosPersona(this);
		}
		System.out.println("Estoy revisando vecinos " + this.toString());
		setTicksPartidoBloqueado(ticksPartidoBloqueado - 1);
		
	}

	public void mover() {
		Ciudad.moverEntidad(this, Direccion.random());
		System.out.println("Me estoy moviendo " + this.toString());
	}

	public int getTicksPartidoBloqueado() {
		return ticksPartidoBloqueado;
	}

	public void setTicksPartidoBloqueado(int ticksPartidoBloqueado) {
		if (ticksPartidoBloqueado < 0) {
			this.ticksPartidoBloqueado = 0;
		} else {
			this.ticksPartidoBloqueado = ticksPartidoBloqueado;
		}
	}
	
	public void placeComponentes() {

		while (true) {

			Celda[][] grilla = Ciudad.getCeldas();

			double probability = BackendController.random.nextDouble() * 3;

			int x = BackendController.random.nextInt(grilla.length - 2);
			int y = BackendController.random.nextInt(grilla[0].length - 2);

			if (probability <= 1.5) {

			boolean square = grilla[x][y] == null && grilla[x][y + 1] == null && grilla[x + 1][y] == null && grilla[x + 1][y + 1] == null;
			boolean l1 = grilla[x][y] != null && grilla[x][y + 1] == null && grilla[x + 1][y] == null && grilla[x + 1][y + 1] == null;
			boolean l2 = grilla[x][y] == null && grilla[x][y + 1] != null && grilla[x + 1][y] == null && grilla[x + 1][y + 1] == null;
			boolean l3 = grilla[x][y] == null && grilla[x][y + 1] == null && grilla[x + 1][y] != null && grilla[x + 1][y + 1] == null;
			boolean l4 = grilla[x][y] == null && grilla[x][y + 1] == null && grilla[x + 1][y] == null && grilla[x + 1][y + 1] != null;
			boolean i1 = grilla[x][y] != null && grilla[x][y + 1] != null && grilla[x + 1][y] == null && grilla[x + 1][y + 1] == null;
			boolean i2 = grilla[x][y] == null && grilla[x][y + 1] != null && grilla[x + 1][y] == null && grilla[x + 1][y + 1] != null;
			
				if (square || l1 || l2 || l3 || l4 || i1 || i2) {
					
					for (int i = x; i < x + 2; i++) {
						for (int j = y; j < y + 2; j++) {
							if (grilla[i][j] == null){
								Celda celda = new Celda(new Point(i, j), this);
								grilla[i][j] = celda;
								this.componentes.add(celda);	
							}
						}
					}

					break;
				}
			} else if (probability > 1.5) {

				if (grilla[x][y] == null && grilla[x][y + 1] == null && grilla[x][y + 2] == null &&
						grilla[x + 1][y] == null && grilla[x + 1][y + 1] == null && grilla[x + 1][y + 2] == null &&
						grilla[x + 2][y] == null && grilla[x + 2][y + 1] == null && grilla[x + 2][y + 2] == null) {

					for (int i = x; i < x + 3; i++) {
						for (int j = y; j < y + 3; j++) {
							if (grilla[i][j] == null){
								Celda celda = new Celda(new Point(i, j), this);
								grilla[i][j] = celda;
								this.componentes.add(celda);	
							}
						}
					}

					break;
				}
			}
		}
	}
	
	@Override
	public String getTipo() {
		return partido.toString();
	}
	
	public int getNumeroPartido () {
		return partido.getCode();
	}

	public NombrePartido getPartido() {
		return partido;
	}
	
	public void setPartido(NombrePartido partido) {
		this.partido = partido;
	}
}