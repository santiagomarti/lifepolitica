package Test.Backend;

import java.util.HashMap;
import java.util.Observable;

public class ParametrosIniciales extends Observable{

	private static volatile ParametrosIniciales instance = null;
	private int velocidad;

	private HashMap<Integer, NombrePartido> mapIdPartido = new HashMap<Integer, NombrePartido>();
	private HashMap<Integer, Integer> mapIdPorcentaje = new HashMap<Integer, Integer>();

	private ParametrosIniciales() {

	}

	public static ParametrosIniciales getInstance() {
		if (instance == null) {
			synchronized (ParametrosIniciales.class) {
				if (instance == null) {
					instance = new ParametrosIniciales();
				}
			}
		}
		return instance;
	}

	public boolean setParametros(int velocidad, int numPartidos,
			HashMap<Integer, Integer> porPartidos) {

		for (int i = 1; i <= numPartidos; i++){
			switch (i){
			case 1: this.mapIdPartido.put(i - 1, NombrePartido.ComuNAUS); break;
			case 2: this.mapIdPartido.put(i - 1, NombrePartido.SoliSoli); break;
			case 3: this.mapIdPartido.put(i - 1, NombrePartido.ElGremio); break;
			case 4: this.mapIdPartido.put(i - 1, NombrePartido.Creceres); break;
			}
		}
		this.velocidad = velocidad;
		this.mapIdPorcentaje = porPartidos;

		return true;
	}
	
	public synchronized void setPorcentaje(int partido, int porcentaje){
		getPorcentajes().put(partido, porcentaje);
		this.setChanged();
		this.notifyObservers();
	}

	public int getVelocidad() {
		return velocidad;
	}

	public synchronized HashMap<Integer, Integer> getPorcentajes() {
		return mapIdPorcentaje;
	}
	
	public NombrePartido getPartidoRandom(){
		int r = BackendController.random.nextInt(getNumeroDePartidos());
		return mapIdPartido.get(r);
	}

	public int getNumeroDePartidos() {
		return mapIdPartido.size();
	}
	
	public HashMap<Integer, NombrePartido> getPartidos() {
		return this.mapIdPartido;
	}

	public void setPartidos(HashMap<Integer, NombrePartido> mapIdPartido) {
		this.mapIdPartido = mapIdPartido;
	}
}