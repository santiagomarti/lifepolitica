package Test.Backend;

public class PartidoPolitico {

	private NombrePartido nombre;
	private int adherentes;

	public PartidoPolitico(NombrePartido nombre){
		this.nombre = nombre;
	}
	
	public NombrePartido getNombre() {
		return nombre;
	}

	public int getAdherentes() {
		return adherentes;
	}
	
	public void setAdherentes(int adherentes) {
		this.adherentes = adherentes;
	}

	public Integer getValorPrimario() {
		// TODO Auto-generated method stub
		return null;
	}
}