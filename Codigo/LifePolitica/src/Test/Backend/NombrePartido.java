package Test.Backend;

public enum NombrePartido {

	ComuNAUS(0), SoliSoli(1), ElGremio(2), Creceres(3);
	
	private int code;
	
	private NombrePartido (int code) {
		this.code = code;
	}
	
	public int getCode () {
		return code;
	}
	
	public static NombrePartido getByIndex(int index){
		switch (index){
		case 0: return ComuNAUS;
		case 1: return SoliSoli;
		case 2: return ElGremio;
		case 3: return Creceres;
		default: return ComuNAUS;
		}
	}
	
	public static NombrePartido random() {
		
		int numPartidos = ParametrosIniciales.getInstance().getNumeroDePartidos();
		int probability = BackendController.random.nextInt(numPartidos);
		
		switch (probability) {
		case 0:
			return NombrePartido.ComuNAUS;
		case 1:
			return NombrePartido.SoliSoli;
		case 2:
			return NombrePartido.ElGremio;
		default:
			return NombrePartido.Creceres;
		}
	}
}
