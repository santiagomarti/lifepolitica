package Test.Backend;


import javax.swing.table.AbstractTableModel;

import Test.LogicaCiudad.Celda;

@SuppressWarnings("serial")
public class ModeloTabla extends AbstractTableModel{
	
	private String[] columnNames;
	
	Celda[][] data;
    
	public ModeloTabla(Celda[][] celdas){
		this.data = celdas;
		
		this.columnNames = new String[celdas.length];
	}
	
	public Class<? extends Object> getColumnClass( int column ) 
    {
        return getValueAt(0, column).getClass();
    }
    
    public int getColumnCount() 
    {
        return columnNames.length;
    }
    
    public String getColumnName( int column ) 
    {
        return columnNames[column];
    }

    public int getRowCount() 
    {
        return data.length;
    }

    public Object getValueAt( int row, int column ) 
    {
        return data[row][column];
    }	
}