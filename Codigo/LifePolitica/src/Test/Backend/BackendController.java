package Test.Backend;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import Test.EventosExternos.EventoExterno;
import Test.LogicaCiudad.Ciudad;
import Test.LogicaCiudad.Persona;

public class BackendController {

	private Simulador simulador;
	private long startTime;
	public int tiempoEspera;
	public Ciudad ciudad;

	public static Random random = new Random();
	
	public BackendController() {
	}

	public void iniciarSimulacionTest(BackendController bC, int vel, int partidos, HashMap<Integer, Integer> porcs, int tiempoEspera)
	{
		this.tiempoEspera = tiempoEspera;
		ParametrosIniciales.getInstance().setParametros(vel, partidos, porcs);
		InitiateSimulationEvent sim = new InitiateSimulationEvent(bC);
		sim.setPercentages(porcs);
		sim.setPartidos(partidos);
		sim.setVelocidad(vel);
		sim.actionPerformed(null);
	}
	public boolean ingresarParametrosIniciales(int vel, int numPart, HashMap<Integer, Integer> porcsParts) {
		return ParametrosIniciales.getInstance().setParametros(vel, numPart, porcsParts);
	}


	public void iniciarSimulacion(int tiempoEspera) {
		Simulador.iniciarSimulacion(tiempoEspera);
	}

	public void setSimulador(Simulador simulador) {
		this.simulador = simulador;
	}

	public void guardarEstado() {
		Simulador.guardarEstado();
	}

	public Simulador getSimulador() {
		return simulador;
	}
	
	public long getStartTime () {
		return startTime;
	}
	
	public void setStartTime (long n) {
		this.startTime = n;
	}
}

class InitiateSimulationEvent implements ActionListener {

	private BackendController backendController;
	
	private HashMap<Integer, Integer> percentages;
	public HashMap<Integer, Integer> getPercentages() {
		return percentages;
	}

	public void setPercentages(HashMap<Integer, Integer> percentages) {
		this.percentages = percentages;
	}

	private int velocidad;
	
	public int getVelocidad() {
		return velocidad;
	}

	public void setVelocidad(int velocidad) {
		this.velocidad = velocidad;
	}

	public int getPartidos() {
		return partidos;
	}

	public void setPartidos(int partidos) {
		this.partidos = partidos;
	}

	private int partidos;
	
	public InitiateSimulationEvent(BackendController backendController) {
		this.backendController = backendController;
	}

	public void actionPerformed(ActionEvent e) {
		this.backendController.setStartTime(System.currentTimeMillis());
		this.backendController.iniciarSimulacion(this.backendController.tiempoEspera);
	
	}
}

class ExternalEvent implements ActionListener {

	private EventoExterno eventoExterno;
	private ArrayList<Persona> ciudadanos;
	private NombrePartido partido;
	
	public ExternalEvent(BackendController backendController) {
		backendController.getSimulador();
		this.ciudadanos = Simulador.ciudad.getPersonas();
	}

	public void actionPerformed(ActionEvent e) {
		this.eventoExterno.execute(this.ciudadanos, this.partido);
	}
}

