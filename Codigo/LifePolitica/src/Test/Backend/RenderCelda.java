package Test.Backend;


import java.awt.Component;
import java.awt.Color;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import Test.LogicaCiudad.Celda;

@SuppressWarnings("serial")
public class RenderCelda extends DefaultTableCellRenderer {
	
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) 
    {
        Component cell = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                
        if( value instanceof Celda )
        {
        	Celda celda = (Celda)value;
        	        	
        	String partido = "calle";
        	
        	if (celda.getParent() != null){
        		partido = celda.getParent().getTipo();
        	}
        	          
            if( partido.equals("ComuNAUS") )
            {
                cell.setBackground(Color.GREEN);
                cell.setForeground(Color.GREEN);
            }
            else if(partido.equals("SoliSoli"))
            {
            	cell.setBackground(Color.RED);
                cell.setForeground(Color.RED);
            }
            else if(partido.equals("ElGremio"))
            {
            	cell.setBackground(Color.ORANGE);
                cell.setForeground(Color.ORANGE);
            }
            else if(partido.equals("Creceres"))
            {
            	cell.setBackground(Color.BLUE);
                cell.setForeground(Color.BLUE);
            }
            else if(partido.equals("calle"))
            {
            	cell.setBackground(Color.GRAY);
                cell.setForeground(Color.GRAY);
            }
            else
            {
              cell.setBackground(Color.BLACK);
              cell.setForeground(Color.BLACK);
            } 
        }
        
        return cell;
    }
}