package Test.MainTests;

import static org.junit.Assert.*;

import java.util.HashMap;

import org.junit.Test;

import Test.Backend.BackendController;
import Test.Backend.Simulador;
import Test.LogicaCiudad.Persona;


/** Test dedicado a revisar que toda entidad tenga un partido
 *  asignado. Los assertions son revisados una vez que finaliza
 *  la simulacion.
 */
public class Test3 {
	
	private static final int VELOCIDAD = 50;
	private static final int PARTIDOS = 2;
	private HashMap<Integer, Integer> porcentajes;
	private static final int TIEMPO_TEST_MILISEGUNDOS = 2000;
	
	@Test
	public void doTest()
	{
		iniciarSimulacion().getSimulador();
		for(Persona p : Simulador.getCiudad().getPersonas())
		{
			if(p.getPartido() != null)
				assertEquals(true, true);
			else assertEquals(true, false);
		}
	}
	
	public BackendController iniciarSimulacion()
	{
		porcentajes = new HashMap<Integer, Integer>();
		setPorcentajes();
		BackendController backendController = new BackendController();		
		backendController.iniciarSimulacionTest(backendController, VELOCIDAD, PARTIDOS, porcentajes, TIEMPO_TEST_MILISEGUNDOS);
		
		return backendController;
	}
	
	public void setPorcentajes()
	{
		switch(PARTIDOS)
		{
		case 1:
			porcentajes.put(0,100);
			break;
		case 2:
			porcentajes.put(0,50);
			porcentajes.put(1,50);
			break;
		case 3:
			porcentajes.put(0,33);
			porcentajes.put(1,33);
			porcentajes.put(2,34);
			break;
		case 4:
			porcentajes.put(0,25);
			porcentajes.put(1,25);
			porcentajes.put(2,25);
			porcentajes.put(3,25);
			break;
			default:
				
		}
	}
}
