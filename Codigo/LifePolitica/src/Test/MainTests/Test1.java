package Test.MainTests;

import static org.junit.Assert.*;

import java.util.HashMap;

import org.junit.Test;

import Test.Backend.BackendController;
import Test.Backend.Simulador;



/** Clase que testea que las Personas (entidades) no mueran durante la simulacion.
 *  El numero de entidades esta fijo en 100. De esta manera, se setea un tiempo de
 *  de duracion, durante el cual se corre la simulacion. Al final de este, el programa
 *  revisa que los threads que contienen a las entidades sigan vivos y comprueba
 *  el assert.
 */
public class Test1 {
	
	private static final int VELOCIDAD = 100;
	private static final int PARTIDOS = 4;
	private HashMap<Integer, Integer> porcentajes;
	private static final int TIEMPO_TEST_MILISEGUNDOS = 2000;

	@Test
	public void doTest (){	
		
		
		int entidadesVivas = 0;
		iniciarSimulacion().getSimulador();
		for(Thread t : Simulador.getCiudad().getThreads())
		{
			if(t.isAlive())
			{
				entidadesVivas++;
			}
		}
		assertEquals(100, entidadesVivas);
	}
	
	public BackendController iniciarSimulacion()
	{
		porcentajes = new HashMap<Integer, Integer>();
		setPorcentajes();
		BackendController backendController = new BackendController();		
		backendController.iniciarSimulacionTest(backendController, VELOCIDAD, PARTIDOS, porcentajes, TIEMPO_TEST_MILISEGUNDOS);
		
		return backendController;
	}
	
	public void setPorcentajes()
	{
		switch(PARTIDOS)
		{
		case 1:
			porcentajes.put(0,100);
			break;
		case 2:
			porcentajes.put(0,50);
			porcentajes.put(1,50);
			break;
		case 3:
			porcentajes.put(0,33);
			porcentajes.put(1,33);
			porcentajes.put(2,34);
			break;
		case 4:
			porcentajes.put(0,25);
			porcentajes.put(1,25);
			porcentajes.put(2,25);
			porcentajes.put(3,25);
			break;
			default:
				
		}
	}
}