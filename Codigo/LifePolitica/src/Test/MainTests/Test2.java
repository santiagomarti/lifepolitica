package Test.MainTests;


import java.util.HashMap;

import org.junit.Test;

import Test.ArchivosTest2.BackendControllerTest2;

/** Test que revisa que, al moverse una entidad, esta se mueva
 *  efectivamente a un lugar donde no hay otras entidades, si
 *  no un espacio vacio. Los asserts se revisan durante la 
 *  simulacion.
 */
public class Test2 {

	private static final int VELOCIDAD = 43;
	private static final int PARTIDOS = 1;
	private HashMap<Integer, Integer> porcentajes;
	private static final int TIEMPO_TEST_MILISEGUNDOS = 2000;
	
	@Test
	public void doTest()
	{
		try
		{
			porcentajes = new HashMap<Integer, Integer>();
		
		setPorcentajes();
		BackendControllerTest2 backendController = new BackendControllerTest2();		
		backendController.iniciarSimulacionTest(backendController, VELOCIDAD, PARTIDOS, porcentajes, TIEMPO_TEST_MILISEGUNDOS);
		} 
		catch(AssertionError e)
		{
			e.printStackTrace();
		}
	}
	
	public void setPorcentajes()
	{
		switch(PARTIDOS)
		{
		case 1:
			porcentajes.put(0,100);
			break;
		case 2:
			porcentajes.put(0,50);
			porcentajes.put(1,50);
			break;
		case 3:
			porcentajes.put(0,33);
			porcentajes.put(1,33);
			porcentajes.put(2,34);
			break;
		case 4:
			porcentajes.put(0,25);
			porcentajes.put(1,25);
			porcentajes.put(2,25);
			porcentajes.put(3,25);
			break;
			default:
				
		}
	}
}
