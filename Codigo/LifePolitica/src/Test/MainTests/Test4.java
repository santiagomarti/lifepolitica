package Test.MainTests;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.HashSet;
import java.util.concurrent.CountDownLatch;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.junit.Test;

import Backend.LogicaCiudad.NombrePartido;
import Backend.LogicaCiudad.NombreShape;
import Backend.LogicaCiudad.Persona;
import Backend.Migracion.MigrationClient;
import Backend.Migracion.MigrationServer;

/** Test que comprueba que las entidades que son enviadas, puedan ser
 * recibidas integramente, es decir, se mantiene la informacion en la
 * ida y vuelta a otros mundos (siempre y cuando el otro mundo tambien
 * mantenga la informacion integra.
 * 
 * @author Grupo 10
 *
 */
public class Test4 {

	private MigrationServer server;
	private Persona testObject;
	private Persona returnObject;
	
	@Test
	public void doTest()
	{
		new MigrationClient();
		server = new MigrationServer();
		testObject = new Persona();
		returnObject = new Persona();
		
		testObject.setPartido(NombrePartido.ComuNAUS, true);
		testObject.setShape(NombreShape.BigSquare);
		
		HashSet<Persona> hashes = new HashSet<Persona>();
		
		hashes.add(testObject);
		
		MigrationClient.setHostName("127.0.0.1");
		MigrationClient.setHostPort(MigrationServer.PUERTO);
		
		final CountDownLatch latch = new CountDownLatch(1);
		final CountDownLatch latchRetorno = new CountDownLatch(1);
		
		Thread t = new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					returnObject = server.initServer(latch);
					latchRetorno.countDown();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});

		t.start();
		try {
			latch.await();
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		try {
			MigrationClient.sendEntity(testObject);
		} catch (ParserConfigurationException | TransformerException e) {
			e.printStackTrace();
		}
		
		try {
			latchRetorno.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		boolean contains = hashes.add(returnObject);
		
		assertEquals(true, contains);	
		
		if(contains) System.out.println("Test exitoso, las entidades son iguales.");
		else System.out.println("Test no exitoso.");
	}
}
