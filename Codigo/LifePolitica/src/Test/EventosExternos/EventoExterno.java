package Test.EventosExternos;


import java.util.List;

import Test.Backend.NombrePartido;
import Test.LogicaCiudad.Persona;


public interface EventoExterno {
	
	public void execute(List<Persona> personas, NombrePartido partido); 

}
