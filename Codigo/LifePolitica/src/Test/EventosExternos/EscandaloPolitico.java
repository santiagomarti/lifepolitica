package Test.EventosExternos;


import java.util.List;

import Test.Backend.BackendController;
import Test.Backend.NombrePartido;
import Test.Backend.ParametrosIniciales;
import Test.LogicaCiudad.Persona;


public class EscandaloPolitico implements EventoExterno{

	@Override
	public synchronized void execute(List<Persona> ciudadanos, NombrePartido partido) {
		for (Persona persona : ciudadanos){
			if (persona.getPartido().equals(partido)){
				double randomNumber = BackendController.random.nextDouble();
				
				if (randomNumber < 0.33){
					int porcentaje = ParametrosIniciales.getInstance().getPorcentajes().get(persona.getNumeroPartido());
					ParametrosIniciales.getInstance().setPorcentaje(persona.getNumeroPartido(), porcentaje - 1);
					
					persona.setPartido(NombrePartido.random());
					
					porcentaje = ParametrosIniciales.getInstance().getPorcentajes().get(persona.getNumeroPartido());
					ParametrosIniciales.getInstance().setPorcentaje(persona.getNumeroPartido(), porcentaje + 1);
				}
			}
		}
	}
}
